FROM node:22-alpine3.18

WORKDIR /opt/app/snowsync

ARG VITE_REACT_APP_API_URL

RUN echo "VITE_REACT_APP_API_URL=${VITE_REACT_APP_API_URL}" > .env

COPY package*.json ./ 

RUN npm install

COPY . .